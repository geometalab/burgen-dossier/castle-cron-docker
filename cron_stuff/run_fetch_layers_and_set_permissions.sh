#!/bin/bash
set -ex

python3 fetch_layers.py --output_dir=/var/data/results

chown -R nginx:nginx /var/data/results
