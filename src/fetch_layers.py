# Copyright 2019 by Sascha Brawer. Licensed under the MIT license.
# SPDX-License-Identifier: MIT
#
# Tool for fetching layers of notable features from OpenStreetMap.
# This will be used for a hackathon in May 2019 about castles in Switzerland,
# organized in collaboration with Wikimedia Foundation Switzerland,
# whose goal it is to improve Swiss castles in Wikipedia. The tool
# can be extended to handle additional layers and regions.
#
# The program sends a query to the OSM Overpass API and formats the result
# in GeoJSON format. The resulting files are written into a place accessible
# to a webserver running on the same machine, so that clients can access them.
# Obviously, this simple approach will not work for layers that have lots
# features, but for the few hundred castles in Switzerland, it should do.
#
# This tool should be run as a cron job, eg. once per hour or so. Invocation:
# $ python fetch_layers.py --output_path=/path/to/public_www_directory

from __future__ import print_function, unicode_literals

import argparse, codecs, collections, gzip, itertools, json
import multiprocessing, os, shutil, tempfile, urllib
import http.client, urllib.parse, urllib.request

from datetime import datetime, timezone
from dateutil.parser import parse as parse_timestamp  # 3rd-party package

urlopen = urllib.request.urlopen
urlquote = urllib.parse.quote_plus

REGIONS = {
    # ISO 3166-1/2: 'CH' for Switzerland; 'DE-BY' for Bavaria
    "CH": (45.6, 5.4, 47.99, 11.2),
}

LAYERS = {
    "castles": [
        "nwr[historic=castle]",
        "nwr[historic=tower]",
        "nwr[historic=archaeological_site][site_type=fortification]",
        "nwr[archaelogical_site=fortification]",
    ],
}

OVERPASS_ENDPOINT = os.getenv(
    "OVERPASS_ENDPOINT", "http://overpass-api.de/api/interpreter"
)


def main():
    script_dir = os.path.dirname(os.path.realpath(__file__))
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        "--output_dir", help="path to directory for storing output files"
    )
    args = argparser.parse_args()
    if not os.path.exists(args.output_dir):
        os.mkdir(args.output_dir)
    qrank = fetch_wikidata_qrank(args.output_dir)
    for layer, region in itertools.product(LAYERS.keys(), REGIONS.keys()):
        fetch_layer_with_timeout(
            layer,
            region,
            args.output_dir,
            wikidata_rank=qrank,
            timeout_seconds=300,
        )  # 5 minutes = 300 seconds


def fetch_layer_with_timeout(layer, region, output_dir, wikidata_rank, timeout_seconds):
    fetch_start = datetime.now(timezone.utc)
    logrec = {
        "fetch_start_timestamp": fetch_start.isoformat(),
        "layer": layer,
        "region": region,
    }
    p = multiprocessing.Process(
        target=fetch_layer, args=(layer, region, logrec, wikidata_rank, output_dir)
    )
    timed_out = False
    p.start()
    p.join(timeout=timeout_seconds)
    if p.is_alive():
        p.terminate()
        timed_out = True
    if p.exitcode != 0:
        logrec.update(
            {
                "fetch_duration_seconds": (
                    datetime.now(timezone.utc) - fetch_start
                ).total_seconds(),
                "fetch_status": "timeout" if timed_out else "crash",
            }
        )
        append_logrecord(logrec, output_dir)


def fetch_layer(layer, region, logrec, qrank, output_dir):
    fetch_start = parse_timestamp(logrec["fetch_start_timestamp"])
    query = build_overpass_query(layer, region)
    url = OVERPASS_ENDPOINT + "?data=" + urlquote(query, safe=".:;/()")
    content, fetch_status = fetch_url(url)
    logrec["fetch_duration_seconds"] = (
        datetime.now(timezone.utc) - fetch_start
    ).total_seconds()
    logrec["fetch_http_status_code"] = fetch_status
    status = "fail"
    if fetch_status == 200:
        overpass_response = json.loads(content)
        entities = extract_wikidata_entities(overpass_response)
        ranking = read_wikidata_rank(qrank, entities)
        geojson = overpass_to_geojson(overpass_response, ranking)
        logrec.update(summarize_stats(geojson))
        # Only use results when Overpass actually returned any features.
        # Sometimes, the API returns empty results in an HTTP reply
        # with "200 OK" status; let's work around this.
        if len(geojson["features"]) > 0:
            geojson_blob = format_json(geojson).encode("utf-8")
            logrec["output_bytes"] = len(geojson_blob)
            filepath = os.path.join(output_dir, "osm-%s-%s.geojson" % (layer, region))
            changed = replace_file_content(filepath, geojson_blob)
            logrec["output_changed"] = changed
            status = "ok"
        else:
            status = "fail_nofeatures"
    logrec["fetch_status"] = status
    append_logrecord(logrec, output_dir)


def fetch_wikidata_qrank(output_dir):
    """Fetches Wikidata QRank file with current rankings of all entities.

    The fetch is conditional on the server-returned ETag as per RFC 7232:
    if the ranking file did not change since our last download, the server
    returns a "304 Not Modified" response, and we use the cached content.
    We download the 100 MB file only if it actually changed upstream.
    """
    qrank_url = "https://qrank.wmcloud.org/download/qrank.csv.gz"
    qrank_path = os.path.join(output_dir, "qrank.csv.gz")
    etag, etag_path = None, os.path.join(output_dir, "qrank-etag.json")
    if os.path.exists(qrank_path) and os.path.exists(etag_path):
        with open(etag_path, "rb") as f:
            etag = json.loads(f.read()).get('ETag')
    headers = {'If-None-Match': etag} if etag else {}
    req = urllib.request.Request(qrank_url, headers=headers)
    try:
        connection = urllib.request.urlopen(req)
    except urllib.error.HTTPError as err:
        if err.code == 304:  # HTTP 304 Not Modified
            return qrank_path
        raise err
    if connection.code == 200:  # HTTP 200 OK
        # Write to a temporary file so cached "qrank.csv.gz" does not end up
        # in a partially fetched state when network breaks during download.
        with open(qrank_path+".tmp", "wb") as qrank_file:
            shutil.copyfileobj(connection, qrank_file)
        etag = connection.info().get('ETag', '')
        with open(etag_path, "w", encoding="utf-8") as etag_file:
            etag_file.write(format_json({'ETag': etag}))
        # Atomically rename temporary file to final output file.
        os.rename(qrank_path+".tmp", qrank_path)
    connection.close()
    return qrank_path


def extract_wikidata_entities(overpass_response):
    """Extract Wikidata entities from an OSM Overpass API response."""
    entities = set()
    for element in overpass_response['elements']:
        entity = element['tags'].get('wikidata')
        if entity:
            entities.add(entity)
    return entities


def read_wikidata_rank(path, needed_entities):
    result = {}
    for line in gzip.open(path):
        line = line.decode('utf-8').strip()
        if len(line) == 0 or line[0] != "Q":
            continue
        # TODO: QRank is moving from tab-separated to comma-separated values;
        # the following line can be removed after March 31, 2021.
        line = line.replace('\t', ',')
        id, rank = line.split(',')
        if id in needed_entities:
            result[id] = float(rank)
    return result


def summarize_stats(fcoll):
    """Compute summary statistics for a GeoJSON feature collection."""
    assert fcoll["type"] == "FeatureCollection"
    geometry_type = collections.Counter(
        [f["geometry"]["type"] for f in fcoll["features"]]
    )
    tags = collections.Counter()
    for f in fcoll["features"]:
        tags.update(f["properties"].keys())
    return {
        "geometry_type_most_common": dict(geometry_type.most_common(1000)),
        "tags_most_common": dict(tags.most_common(1000)),
        "tags_total": len(tags),
        "osm_base_timestamp": fcoll["properties"]["osm_base_timestamp"],
    }


def fetch_url(url):
    # with open('cached-results.json', 'rb') as f: return (f.read(), 200)
    try:
        connection = urlopen(url)
        content = connection.read()
        connection.close()
        # with open('cached-results.json', 'wb') as f: f.write(content)
        return (content, connection.code)
    except:
        return (b"", 520)  # unofficial code (eg. Cloudflare) for network errs


def replace_file_content(filepath, content):
    """Atomially replace filepath by new content if content is new."""
    assert isinstance(content, bytes)
    # If content has not changed, we do not touch the existing file
    # so its timestamp stays the same.
    if os.path.exists(filepath):
        with open(filepath, "rb") as old_file:
            old_content = old_file.read()
        if content == old_content:
            return False  # no change
    basename = os.path.basename(filepath)
    suffix = basename.rsplit(".", 1)[-1] if "." in basename else ".tmp"
    tmpfd, tmpname = tempfile.mkstemp(
        dir=os.path.dirname(filepath), prefix="tmp-", suffix=suffix
    )
    with os.fdopen(tmpfd, "wb") as out:
        out.write(content)
    os.rename(tmpname, filepath)
    return True  # content has changed


def append_logrecord(rec, output_dir):
    filepath = os.path.join(output_dir, "fetch_layers.log")
    with open(filepath, "ab") as f:
        f.write(format_json(rec).replace("\n", " ").encode("utf-8"))
        f.write(b"\n")
        f.flush()
        os.fsync(f.fileno())


def build_overpass_query(layer, region):
    coverage = "(%s,%s,%s,%s)" % REGIONS[region]
    tags = LAYERS[layer]
    query_part = ""
    for tag in tags:
        query_part += "%s%s;" % (tag, coverage)
    query_pattern = "[out:json][timeout:240];(%(query_part)s);out geom;"
    query = query_pattern % {"query_part": query_part}
    return query


def overpass_to_geojson(op, wikidata_rank):
    elements = op["elements"]
    features = []
    for node in filter(lambda e: e["type"] == "node", elements):
        tags = dict(node["tags"])
        features.append(
            {
                "type": "Feature",
                "id": "N%s" % node["id"],
                "geometry": {
                    "type": "Point",
                    "coordinates": lonlat(node),
                },
                "properties": tags,
            }
        )
    for way in filter(lambda e: e["type"] == "way", elements):
        geometry = way["geometry"]
        tags = dict(way["tags"])
        closed = geometry[0] == geometry[-1]
        coords = [lonlat(p) for p in geometry]
        if closed:
            coords = fix_ring_direction("outer", coords)
        features.append(
            {
                "type": "Feature",
                "id": "W%s" % way["id"],
                "geometry": {
                    "type": "Polygon" if closed else "LineString",
                    "coordinates": [coords] if closed else coords,
                },
                "properties": tags,
            }
        )
    for rel in filter(lambda e: e["type"] == "relation", elements):
        features.append(overpass_relation_to_geojson(rel))
    features.sort(key=lambda f: get_sortkey(f, wikidata_rank))
    return {
        "type": "FeatureCollection",
        "features": features,
        "properties": {"osm_base_timestamp": op["osm3s"]["timestamp_osm_base"]},
    }


def get_sortkey(f, wikidata_rank):
    """Compute a sort key for a GeoJSON feature. Important features come first.

    For ranking OSM features, we look at how many pageviews the corresponding
    pages had on any Wikimedia projects (Wikipedia, Wikivoyage, etc.) in any
    language. Occasionally, several features have the same rank according to
    this metric. In that case, we rank by Wikidata ID, assuming that people
    have created Wikidata entities for well-known things before they created
    entities for the long tail. It does not matter much which heuristic we
    use; the main goal of the secondary sort key is to make the sort order
    stable across multiple invocations of the script.
    """
    wikidata_id = f["properties"].get("wikidata", "").strip()
    if not wikidata_id.startswith("Q"):
        return (0, 1e20)  # 0 views, high ID number
    # negative rank so that large viewcounts come first when sorting
    return (-wikidata_rank.get(wikidata_id, 0), int(wikidata_id[1:]))


def overpass_relation_to_geojson(rel):
    tags = dict(rel["tags"])
    members = rel["members"]
    outer = [m for m in members if m["role"] == "outer" and m["type"] == "way"]
    if len(outer) != 1:  # we don't handle multipolygons etc., return a point
        bounds = rel["bounds"]
        minlon, maxlon = bounds["minlon"], bounds["maxlon"]
        minlat, maxlat = bounds["minlat"], bounds["maxlat"]
        lon = minlon + (maxlon - minlon) / 2
        lat = minlat + (maxlat - minlat) / 2
        return {
            "type": "Feature",
            "id": "R%s" % rel["id"],
            "geometry": {
                "type": "Point",
                "coordinates": [lon, lat],
            },
            "properties": tags,
        }
    assert len(outer) == 1, tags
    outer_ring = [lonlat(p) for p in outer[0]["geometry"]]
    rings = [fix_ring_direction("outer", outer_ring)]
    for m in members:
        if m["role"] == "inner" and m["type"] == "way":
            inner_ring = [lonlat(p) for p in m["geometry"]]
            rings.append(fix_ring_direction("inner", inner_ring))
    return {
        "type": "Feature",
        "id": "R%s" % rel["id"],
        "geometry": {
            "type": "Polygon",
            "coordinates": rings,
        },
        "properties": tags,
    }


def lonlat(p):
    return [p["lon"], p["lat"]]


def fix_ring_direction(direction, coords):
    # https://stackoverflow.com/questions/1165647
    assert direction in {"outer", "inner"}
    assert coords[0] == coords[-1], "ring must be closed"
    total = 0
    for i in range(len(coords) - 1):
        (Px, Py), (Qx, Qy) = coords[i], coords[i + 1]
        total += (Qx - Px) * (Qy + Py)
    if (direction == "outer" and total > 0) or (direction == "inner" and total <= 0):
        coords.reverse()
    return coords


def format_json(j):
    return json.dumps(j, ensure_ascii=False, sort_keys=True, separators=(",", ":"))


if __name__ == "__main__":
    main()
